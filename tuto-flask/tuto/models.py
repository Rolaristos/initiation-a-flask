import yaml, os.path
from .app import db
from datetime import datetime
from .app import login_manager

Books = yaml.safe_load(
    open(
        os.path.join(
            os.path.dirname(__file__), "data.yml"
        )
    )
)
i=0
for book in Books:
    book['id'] = i
    i+=1

def get_sample():
    return Books[0:10]

class Author(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __repr__ (self):
        return "Author (%d) %s" % (self.id , self.name)
    
    def get_id(self):
        return self.id
    
class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    price = db.Column(db.Float)
    title = db.Column(db.String(100))
    url = db.Column(db.String(250))
    img = db.Column(db.String(200))
    author_id = db.Column(db.Integer, db.ForeignKey("author.id"))
    
    author = db.relationship("Author", backref= db.backref("books",lazy="dynamic"))
    comments = db.relationship('Commentaire', back_populates='book')

    def __repr__ (self ):
        return "Book (%d) %s" % (self.id , self.title)
    
    def nom_auteur(self):
        return self.author

def get_sample_v2():
    return Book.query.limit(10).all()

def get_sample_v3(author_id, author_name):
    new_books = Book.query.filter_by(author_id=author_id).all()
    author_books = []

    for curr_book in new_books:
        author_books.append({
            'author': author_name,
            'img': curr_book.img,
            'price': curr_book.price,
            'title': curr_book.title,
            'url': curr_book.url,
            'id': curr_book.id-1  # Ajout de l'ID du livre
        })

    return author_books

def get_sample_v4():
    all_books = Book.query.all()
    books_list = []

    for curr_book in all_books:
        books_list.append({
            'author': curr_book.nom_auteur().name,
            'img': curr_book.img,
            'price': curr_book.price,
            'title': curr_book.title,
            'url': curr_book.url,
            'id': curr_book.id-1
        })

    return books_list

def get_author(id):
    return Author.query.get(id)

from flask_login import UserMixin

class User(db.Model, UserMixin):
    username = db.Column(db.String(50), primary_key =True)
    password = db.Column(db.String(64))
    
    comments = db.relationship('Commentaire', back_populates='user')
        
    def get_id(self):
        return self.username

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)

class Commentaire(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text)
    book_id = db.Column(db.Integer, db.ForeignKey('book.id'))
    user_id = db.Column(db.String(50), db.ForeignKey('user.username'))
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    book = db.relationship('Book', back_populates='comments')
    user = db.relationship('User', back_populates='comments')
