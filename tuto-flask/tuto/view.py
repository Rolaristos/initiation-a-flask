from .app import app, db
from flask import render_template, url_for, redirect
from .models import get_sample, get_author, get_sample_v3, get_sample_v4
from flask_wtf import FlaskForm
from wtforms import StringField ,HiddenField
from wtforms.validators import DataRequired
from flask_login import login_required, login_user, current_user, logout_user
from flask import request
from .models import Book
from .models import User
from hashlib import sha256
from wtforms import PasswordField
from flask import request, redirect, url_for
from .models import Commentaire, db
from wtforms import FloatField
from .models import Author

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/pas_de_livres")
def pas_de_livres():
    return render_template("pas_de_livres.html")

@app.route("/livres", methods=['GET', 'POST'])
def livres():
    id_usr = None  # Par défaut l'ID est None
    tout_voir = False
    pas_de_livre = False

    if request.method == 'POST':
        tout_voir = request.form.get('tout_voir') == 'True'
    
    if current_user.is_authenticated:
        auteur = Author.query.filter_by(name=current_user.username).first()
        if auteur:
            id_usr = auteur.id
            name = auteur.name
            if get_sample_v3(id_usr, name) == []:
                pas_de_livre = True  
            else:
                pas_de_livre = False

    if tout_voir:
        books = get_sample_v4()
    else:
        books = get_sample()
    
    return render_template(
        "livres.html",
        title="Books",
        books=books,
        tout_voir=tout_voir,
        id_usr=id_usr,
        pas_de_livre=pas_de_livre
    )

@app.route("/detail/<id>")
def detail(id):
    books = get_sample_v4()
    book = books[int(id)]
    ma_page = False

    # Recherche de l'ID de l'auteur en fonction du titre du livre
    book_title = book['title']
    book_author_id = None

    # On récupère l'ID de l'auteur correspondant au titre du livre
    book_obj = Book.query.filter_by(title=book_title).first()
    if book_obj:
        book_author_id = book_obj.author_id

    comments = Commentaire.query.filter_by(book_id=book['id']).all()
    
    if current_user.is_authenticated:
        if current_user.username == book['author']:
            ma_page = True

    return render_template("detail.html", 
            book=book, book_author_id=book_author_id,
            comments=comments, ma_page=ma_page)

class AuthorForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('Nom', validators =[DataRequired()])

@app.route("/auteur/<int:id>")
def auteur(id):
    a = get_author(int(id))
    books = get_sample_v3(id, a.name)
    
    return render_template(
        "auteur.html", books = books,
        author=a)

@app.route('/edit_author/<int:author_id>', methods=['GET', 'POST'])
@login_required
def edit_author(author_id):
    author = Author.query.get(author_id)
    form = AuthorForm(obj=author)
    #Si le numéro d'auteur_id est supérieu au max de autheur_id
    if author_id > Author.query.count():
        return redirect(url_for('home'))

    if form.validate_on_submit():
        form.populate_obj(author)
        db.session.commit()
        return redirect(request.referrer)

    return render_template('edit-author.html', author=author, form=form, author_id=author_id)

class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()
    
    def get_authentificated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None

@app.route("/login", methods=["GET", "POST"])
def login():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    if f.validate_on_submit():
        user = f.get_authentificated_user()
        if user:
            login_user(user)
            next = f.next.data or url_for('home')
            return redirect(next)
    return render_template("login.html", form = f)

class RegistrationForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()
    
    def register_user(self):
        user = User.query.filter_by(username=self.username.data).first()
        if user:
            return False  # L'utilisateur existe déjà

        m = sha256()
        m.update(self.password.data.encode())
        password_hash = m.hexdigest()
        
        # Enregistrez l'utilisateur avec le nom d'utilisateur et le hachage du mot de passe
        new_user = User(username=self.username.data, password=password_hash)
        db.session.add(new_user)
        db.session.commit()
        return True

@app.route("/registration", methods=["GET", "POST"])
def registration():
    f = RegistrationForm()
    if f.validate_on_submit():
        if f.register_user():
            # Rediriger vers la page d'accueil si l'inscription réussit
            return redirect(url_for('home'))
        else:
            # Afficher un message d'erreur si l'utilisateur existe déjà
            error_message = "Cet utilisateur existe déjà. Veuillez choisir un autre nom d'utilisateur."
            return render_template("registration.html", form=f, error_message=error_message)

    return render_template("registration.html", form=f)

@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route('/post_comment/<int:book_id>', methods=['POST'])
def post_comment(book_id):
    if not current_user.is_authenticated:
        return redirect(url_for('login'))

    # On récupère le texte du commentaire du formulaire
    text = request.form.get('commentaire')

    # On crée un nouveau commentaire et l'associe au livre et à l'utilisateur
    comment = Commentaire(text=text, book_id=book_id, user_id=current_user.username)

    db.session.add(comment)
    db.session.commit()

    return redirect(url_for('detail', id=book_id))

class BookForm(FlaskForm):
    title = StringField('Titre du livre', validators=[DataRequired()])
    price = FloatField('Prix', validators=[DataRequired()])
    url = StringField('URL du livre', validators=[DataRequired()])
    img = StringField("URL de l'image du livre", validators=[DataRequired()])
    author = StringField('Auteur', render_kw={'readonly': True})

    def set_author(self, username):
        self.author.data = username

@app.route('/add_book', methods=['GET', 'POST'])
@login_required
def add_book():
    form = BookForm(request.form)
    form.set_author(current_user.username)
    
    if form.validate_on_submit():
        
        title = form.title.data
        
        #Il faut vérifier qu'il n'y a pas de livre portant le meme nom
        book = Book.query.filter_by(title=title).first()
        if book:
            error_message = "Ce livre existe déjà. Veuillez choisir un autre nom de livre."
            return render_template("add_book.html", form=form, error_message=error_message)
        
        price = form.price.data
        url = form.url.data
        img = form.img.data
        # On vérifie si l'utilisateur est déjà un auteur
        author = Author.query.filter_by(name=current_user.username).first()
        
        if not author:
            # Si l'utilisateur n'est pas déjà un auteur, on l'ajoute
            author = Author(name=current_user.username)
            db.session.add(author)
        
        # On crée le livre en utilisant l'auteur (l'utilisateur)
        new_book = Book(title=title, price=price, url=url, img=img, author=author)
        db.session.add(new_book)
        db.session.commit()
        return redirect(url_for('auteur', id=author.id))
    
    return render_template("add_book.html", form=form)


@app.route('/search', methods=['POST'])
def search():
    search_query = request.form.get('search_query')  # Récupérez la requête de recherche depuis le formulaire
    results = []

    # Rechercher les auteurs dont le nom correspond à la requête
    authors = Author.query.filter(Author.name.like(f'%{search_query}%')).all()
    results.extend(authors)

    # Rechercher les livres dont le titre correspond à la requête
    books = Book.query.filter(Book.title.like(f'%{search_query}%')).all()
    results.extend(books)

    return render_template('search_results.html', results=results)

@app.route('/delete_book/<int:id>', methods=['POST'])
def delete_book(id):
    if request.method == 'POST':
        book = Book.query.get(id)
        
        if book:
            comments = Commentaire.query.filter_by(book_id=book.id).all()
            
            for comment in comments:
                db.session.delete(comment)
                
            db.session.delete(book)
            db.session.commit()

    return redirect(url_for('livres'))

@app.route('/delete_comment/<int:comment_id>', methods=['POST'])
@login_required 
def delete_comment(comment_id):
    comment = Commentaire.query.get(comment_id)

    if comment and comment.user_id == current_user.username:
        
        db.session.delete(comment)
        db.session.commit()
    # Redirige vers la page précédente
    return redirect(request.referrer)
